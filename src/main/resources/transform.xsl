<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:xslt="http://xml.apache.org/xslt">
    <xsl:strip-space elements="*"/>
    <xsl:output method="xml" indent="yes" xslt:indent-amount="4"/>

    <xsl:template match="@* | node()">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="entry">
        <xsl:copy>
            <xsl:attribute name="field">
                <xsl:value-of select="./field"/>
            </xsl:attribute>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="field"/>

</xsl:stylesheet>