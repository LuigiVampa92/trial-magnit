package com.luigivampa92.trial.magnit;

import com.luigivampa92.trial.magnit.data.service.BusinessEntityService;
import com.luigivampa92.trial.magnit.mapper.DbToXmlEntityMapper;
import com.luigivampa92.trial.magnit.profiler.Task;
import com.luigivampa92.trial.magnit.xml.*;
import com.luigivampa92.trial.magnit.xml.model.BusinessEntity;
import com.luigivampa92.trial.magnit.xml.model.BusinessEntityList;
import com.luigivampa92.trial.magnit.xml.model.FormattedBusinessEntity;
import com.luigivampa92.trial.magnit.xml.model.FormattedBusinessEntityList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BusinessService {

    @Autowired
    private BusinessEntityService dbService;

    @Autowired
    private MarshallService marshallService;

    @Autowired
    private DbToXmlEntityMapper dxMapper;

    @Task("fill table")
    public void fillTable(int N) throws Exception {
        dbService.clearTable();
        dbService.createEntities(N);
    }

    @Task("convert database records to jaxb objects")
    public List<BusinessEntity> transferDbRecordsToXml() throws Exception {
        List<com.luigivampa92.trial.magnit.data.model.BusinessEntity> entities = dbService.getAllEntities();

        List<BusinessEntity> list = new ArrayList<>();
        for (com.luigivampa92.trial.magnit.data.model.BusinessEntity entity: entities) {
            list.add(dxMapper.map(entity));
        }

        return list;
    }

    @Task("save jaxb objects to 1.xml")
    public void marshallBusinessEntityList(List<BusinessEntity> entities, String fileName) throws Exception {
        BusinessEntityList businessEntities = new BusinessEntityList(entities);
        marshallService.convertListToXml(businessEntities, fileName);
    }

    @Task("transform jaxb object through xslt to 2.xml")
    public void convertXmlThroughXslt(List<BusinessEntity> entities, String fileName) throws Exception {
        BusinessEntityList businessEntities = new BusinessEntityList(entities);
        marshallService.transformObjectThroughXslt(businessEntities, fileName);
    }

    @Task("count sum of node attributes in 2.xml")
    public int getSumOfNodeAttributesFromXml(String fileName) throws Exception {
        FormattedBusinessEntityList list = marshallService.convertXmlToFormattedList(fileName);

        int sum = 0;
        for (FormattedBusinessEntity entity: list.getList()) {
            sum += entity.getField();
        }

        return sum;
    }
}
