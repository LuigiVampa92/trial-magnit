package com.luigivampa92.trial.magnit.xml.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "enrty")
@XmlAccessorType(XmlAccessType.NONE)
public class BusinessEntity implements Serializable {

    @XmlElement
    private Integer field;

    public BusinessEntity() {}

    public BusinessEntity(Integer field) {
        this.field = field;
    }

    public Integer getField() {
        return field;
    }

    public void setField(Integer field) {
        this.field = field;
    }
}
