package com.luigivampa92.trial.magnit.xml;

import com.luigivampa92.trial.magnit.xml.model.BusinessEntityList;
import com.luigivampa92.trial.magnit.xml.model.FormattedBusinessEntityList;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.util.JAXBSource;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.InputStream;

@Component
public class MarshallService {

    private JAXBContext jaxbContext;
    private TransformerFactory transformerFactory;
    private Marshaller marshaller;
    private Unmarshaller unmarshaller;

    public MarshallService() throws Exception {
        jaxbContext = JAXBContext.newInstance(BusinessEntityList.class, FormattedBusinessEntityList.class);
        transformerFactory = TransformerFactory.newInstance();
        marshaller = jaxbContext.createMarshaller();
        unmarshaller = jaxbContext.createUnmarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    }

    public void convertListToXml(BusinessEntityList entity, String fileName) throws JAXBException {
        marshaller.marshal(entity, new File(fileName));
    }

    public BusinessEntityList convertXmlToList(String fileName) throws JAXBException {
        return (BusinessEntityList) unmarshaller.unmarshal(new File(fileName));
    }

    public void convertFormattedListToXml(FormattedBusinessEntityList entity, String fileName) throws JAXBException {
        marshaller.marshal(entity, new File(fileName));
    }

    public FormattedBusinessEntityList convertXmlToFormattedList(String fileName) throws JAXBException {
        return (FormattedBusinessEntityList) unmarshaller.unmarshal(new File(fileName));
    }

    public void transformObjectThroughXslt(BusinessEntityList object, String fileName) throws Exception {
        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("transform.xsl");
        StreamSource xslt = new StreamSource(is);
        Transformer transformer = transformerFactory.newTransformer(xslt);

        JAXBSource source = new JAXBSource(jaxbContext, object);
        StreamResult result = new StreamResult(new File(fileName));

        transformer.transform(source, result);
    }
}
