package com.luigivampa92.trial.magnit.xml.model;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

@XmlRootElement(name = "enrty")
@XmlAccessorType(XmlAccessType.NONE)
public class FormattedBusinessEntity implements Serializable {

    @XmlAttribute
    private Integer field;

    public FormattedBusinessEntity() {}

    public FormattedBusinessEntity(Integer field) {
        this.field = field;
    }

    public Integer getField() {
        return field;
    }

    public void setField(Integer field) {
        this.field = field;
    }
}