package com.luigivampa92.trial.magnit.xml.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "entries")
@XmlAccessorType(XmlAccessType.NONE)
public class FormattedBusinessEntityList implements Serializable {

    @XmlElement(name = "entry")
    private List<FormattedBusinessEntity> list;

    public FormattedBusinessEntityList() {}

    public FormattedBusinessEntityList(List<FormattedBusinessEntity> list) {
        this.list = list;
    }

    public List<FormattedBusinessEntity> getList() {
        return list;
    }

    public void setList(List<FormattedBusinessEntity> list) {
        this.list = list;
    }
}
