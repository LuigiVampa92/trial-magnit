package com.luigivampa92.trial.magnit.xml.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "entries")
@XmlAccessorType(XmlAccessType.NONE)
public class BusinessEntityList implements Serializable {

    @XmlElement(name = "entry")
    private List<BusinessEntity> list;

    public BusinessEntityList() {}

    public BusinessEntityList(List<BusinessEntity> list) {
        this.list = list;
    }

    public List<BusinessEntity> getList() {
        return list;
    }

    public void setList(List<BusinessEntity> list) {
        this.list = list;
    }
}
