package com.luigivampa92.trial.magnit.mapper;

public interface Mapper<F,T> {

    T map(F object);
}
