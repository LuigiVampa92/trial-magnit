package com.luigivampa92.trial.magnit.mapper;

import com.luigivampa92.trial.magnit.xml.model.BusinessEntity;
import org.springframework.stereotype.Component;

@Component
public class XmlToDbEntityMapper implements Mapper<BusinessEntity, com.luigivampa92.trial.magnit.data.model.BusinessEntity> {

    @Override
    public com.luigivampa92.trial.magnit.data.model.BusinessEntity map(BusinessEntity object) {
        com.luigivampa92.trial.magnit.data.model.BusinessEntity entity = new com.luigivampa92.trial.magnit.data.model.BusinessEntity();
        entity.setField(object.getField());

        return entity;
    }
}
