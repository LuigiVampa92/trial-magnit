package com.luigivampa92.trial.magnit.mapper;

import com.luigivampa92.trial.magnit.xml.model.BusinessEntity;
import org.springframework.stereotype.Component;

@Component
public class DbToXmlEntityMapper implements Mapper<com.luigivampa92.trial.magnit.data.model.BusinessEntity, BusinessEntity> {

    @Override
    public BusinessEntity map(com.luigivampa92.trial.magnit.data.model.BusinessEntity object) {
        BusinessEntity entity = new BusinessEntity();
        entity.setField(object.getField());

        return entity;
    }
}
