package com.luigivampa92.trial.magnit.data.service;

import com.luigivampa92.trial.magnit.data.model.BusinessEntity;
import org.hibernate.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BusinessEntityService {

    @Value("${database.batch.size}")
    private Integer batchSize;

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    public List<BusinessEntity> getAllEntities() {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(BusinessEntity.class);
        return criteria.list();
    }

    public void createEntities(int n) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            for (int i=1; i<=n; ++i) {
                session.save(new BusinessEntity(i));
                if (i % batchSize == 0) {
                    session.flush();
                    session.clear();
                }
            }
        }
        finally {
            transaction.commit();
            session.close();
        }
    }

    public void clearTable() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.getNamedQuery("table.magnit_test.clear");
        query.executeUpdate();
    }
}
