package com.luigivampa92.trial.magnit.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MAGNIT_TEST")
public class BusinessEntity {

    @Id
    @Column(name = "FIELD")
    private Integer field;

    public BusinessEntity() {
    }

    public BusinessEntity(Integer field) {
        this.field = field;
    }

    public Integer getField() {
        return field;
    }

    public void setField(Integer field) {
        this.field = field;
    }
}

