package com.luigivampa92.trial.magnit.profiler;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;

@Aspect
@Component
public class TimeProfilerAspect {

    private static final Logger logger = LoggerFactory.getLogger(TimeProfilerAspect.class);

    @Around("execution(* com.luigivampa92.trial.magnit.*.*(..)) && @annotation(com.luigivampa92.trial.magnit.profiler.Task))")
    public Object measureTime(ProceedingJoinPoint pjp) throws Throwable {
        long start = System.currentTimeMillis();
        Object returnObject = pjp.proceed();
        double elapsedTime = ((double) (System.currentTimeMillis() - start)) / 1000;
        logger.info("Task \"{}\" complete. Elapsed time is {} sec.", getTaskName(pjp), elapsedTime);
        return returnObject;
    }

    private String getTaskName(ProceedingJoinPoint pjp) {
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        Annotation[] annotations = signature.getMethod().getDeclaredAnnotations();

        for (Annotation annotation: annotations) {
            if (annotation instanceof Task) {
                return  ((Task) annotation).value();
            }
        }
        return null;
    }
}
