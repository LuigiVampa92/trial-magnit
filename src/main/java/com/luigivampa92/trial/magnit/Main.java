package com.luigivampa92.trial.magnit;

import com.luigivampa92.trial.magnit.profiler.Task;
import com.luigivampa92.trial.magnit.xml.model.BusinessEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Main {

    @Value("${business.number}")
    private Integer N;

    @Autowired
    private BusinessService service;

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    private static final String CONTEXT_PATH = "classpath*:spring-context.xml";

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext(CONTEXT_PATH);
        Main main = context.getBean(Main.class);
        main.run();
    }

    @Task("ALL STEPS")
    public void run() {
        try {
            if (N == null)
                throw new Exception("Number N is not assigned. Set it in property file in resources");

            logger.info("Starting execution. Number N == {}", N);

            // first stage - create N records in db
            service.fillTable(N);

            // second stage - export db records to xml file
            List<BusinessEntity> businessEntities = service.transferDbRecordsToXml();
            service.marshallBusinessEntityList(businessEntities, "1.xml");

            // third stage - transform one xml file to another through xslt
            service.convertXmlThroughXslt(businessEntities, "2.xml");

            // fourth stage - read second xml file, count sum of attribute values
            int sum = service.getSumOfNodeAttributesFromXml("2.xml");

            logger.info("Execution done. Sum is {}", sum);
            System.out.println(sum);
        }
        catch (Exception e) {
            logger.error("Execution failed. Error occured: \"{}\"", e.getMessage());
        }
    }
}
