#Overview

Тестовое задание для Магнита.
Для запуска нужны java, maven, и какое-нибудь соединение с БД. Во время разработки в качестве БД использовалась postgresql-9.4

#Деплой

- Клонируемся:
$ git clone https://LuigiVampa92@bitbucket.org/LuigiVampa92/trial-magnit.git

- Создаем заготовку для логов, в них будут падать сообщения о времени затраченном для выполнения каждого из этапов
$ mkdir logs
$ touch ./logs/common.log

- Переходим в директорию проекта
$ cd trial-magnit

- Редактируем проперти, указываем параметры подключения к постгресу, число N, папку с логами (её создали выше)
$ vim ./src/main/resources/magnit.properties

- Собираем джарку
$ mvn package

- Запускаемся
$ java -jar ./target/magnit-1.0-SNAPSHOT.jar